package com.classpath.springcore.config;

import com.classpath.springcore.dao.InMemoryItemDAOImpl;
import com.classpath.springcore.dao.ItemDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


//@Configuration
//Using Java configuration
//@EnableAspectJAutoProxy
public class AppConfiguration {

    //@Bean("myCustomItemDAO")
    public ItemDAO itemDAO(){
        //If there is a jar with mysql connector create a datasource
        //If there is a bean with datasource, then create a transaction object
        //If there is web.jar in the classpath, create a dispatcher server
        return new InMemoryItemDAOImpl();
    }
}