package com.classpath.springcore.dao;

import com.classpath.springcore.model.Item;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@PropertySource({"classpath:jdbc.properties"})
public class InMemoryItemDAOImpl implements ItemDAO {

    @Value("${jdbc.connectstr}")
    private String connectStr;

    @Value("${jdbc.username}")
    private String username;

    @Value("${jdbc.password}")
    private String password;

    @Override
    public Item save(Item item) {
        System.out.println("Came inside the InMemory DAO Impl class");
        System.out.println("Connect string :: "+ this.connectStr);
        System.out.println("Username :: "+ this.username);
        System.out.println("Password :: "+ this.password);
        return item;
    }

    @Override
    public Set<Item> fetchAll() {
        return null;
    }

    @Override
    public Optional<Item> fetchItemById(long itemId) {
        return Optional.empty();
    }

    @Override
    public void deleteItemById(long itemId) {

    }
}