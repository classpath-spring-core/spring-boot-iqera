package com.classpath.springcore.dao;

import com.classpath.springcore.model.Item;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Repository("itemDAO")
public class ItemDAOImpl implements ItemDAO {

    private static Set<Item> items = new HashSet<>();

    @Override
    public Item save(Item item) {
        items.add(item);
        return item;
    }

    @Override
    public Set<Item> fetchAll() {
        return items;
    }

    @Override
    public Optional<Item> fetchItemById(long itemId) {
        return items.stream().findAny();
    }

    @Override
    public void deleteItemById(long itemId) {
        Optional<Item> itemOption = fetchItemById(itemId);
        if (itemOption.isPresent()){
            items.remove(itemOption.get());
        }
    }


    private void destroy() {
        System.out.println("Inside the destroy method");
    }


    private void init() {
        System.out.println("Inside the init method");
    }
}