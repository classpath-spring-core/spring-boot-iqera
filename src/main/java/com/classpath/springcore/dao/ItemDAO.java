package com.classpath.springcore.dao;

import com.classpath.springcore.model.Item;

import java.util.Optional;
import java.util.Set;

public interface ItemDAO {

    Item save(Item item);

    Set<Item> fetchAll();

    Optional<Item> fetchItemById(long itemId);

    void deleteItemById(long itemId);
}