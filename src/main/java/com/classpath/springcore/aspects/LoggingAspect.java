package com.classpath.springcore.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    //Aspect will contain advices
    @Before("execution(* com.classpath.springcore.service.ItemServiceImpl.*(com.classpath.springcore.model.Item))")
    // execution(modifiers-pattern? ret-type-pattern declaring-type-pattern?name-pattern(param-pattern)throws-pattern?)
    public void methodBeforeLog(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        if(args.length > 0) {
            System.out.println("Came inside the method " + args[0] + "when invoking the method BEFORE:: " + joinPoint.getSignature());
        } else {
            System.out.println("Came inside the method when invoking the method BEFORE:: " + joinPoint.getSignature());
        }
    }

    @After("within(com.classpath.springcore.dao..*)")
    public void methodAfterLog(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        System.out.println("Came inside the method "+ args + "when invoking the method AFTER:: "+ joinPoint.getSignature());
    }
}