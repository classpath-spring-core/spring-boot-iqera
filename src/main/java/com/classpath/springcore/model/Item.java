package com.classpath.springcore.model;

import lombok.*;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "itemId")
@Builder
@AllArgsConstructor
public class Item {

    private long itemId;

    private String itemName;

    private double itemPrice;
}