package com.classpath.springcore.model;

public class ElectronicItem extends Item {

    public ElectronicItem(long itemId, String itemName, double itemPrice) {
        super(itemId, itemName, itemPrice);
    }
}