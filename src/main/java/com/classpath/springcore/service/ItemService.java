package com.classpath.springcore.service;

import com.classpath.springcore.exception.ItemNotFoundException;
import com.classpath.springcore.model.Item;

import java.util.Set;

public interface ItemService {

    Item save(Item item);

    Set<Item> fetchAll();

    Item fetchItemById(long itemId) throws ItemNotFoundException;

    void deleteItemById(long itemId);
}