package com.classpath.springcore.service;

import com.classpath.springcore.dao.ItemDAO;
import com.classpath.springcore.exception.ItemNotFoundException;
import com.classpath.springcore.model.Item;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Set;

@Service("itemService")
@Scope("singleton")
public class ItemServiceImpl implements ItemService {

    private ItemDAO itemDAO;

    public ItemServiceImpl(@Qualifier("itemDAO") ItemDAO itemDAO){
        this.itemDAO = itemDAO;
    }

    @PostConstruct
    public void inititalize(){
        System.out.println("Custom Init method");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("Custom destroy method");
    }

    public Item save(Item item) {
        return this.itemDAO.save(item);
    }

    public Set<Item> fetchAll() {
        return this.itemDAO.fetchAll();
    }

    public Item fetchItemById(long itemId) throws ItemNotFoundException {
        return this.itemDAO.fetchItemById(itemId).orElseThrow(ItemNotFoundException::new);
    }

    public void deleteItemById(long itemId) {
        this.itemDAO.deleteItemById(itemId);
    }
}