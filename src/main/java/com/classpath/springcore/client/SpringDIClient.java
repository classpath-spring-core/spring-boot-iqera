package com.classpath.springcore.client;

import com.classpath.springcore.model.Item;
import com.classpath.springcore.service.ItemService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringDIClient {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");

        ItemService itemService = applicationContext.getBean("itemService", ItemService.class);

        // Using the builder pattern to create objects
        Item iPad = Item.builder().itemId(12).itemName("IPad").itemPrice(29_000).build();
        Item iPhone = Item.builder().itemId(13).itemName("IPhone-12").itemPrice(1_29_000).build();

       // itemService.save(iPad);
        //itemService.save(iPhone);
        itemService.save(iPad);

        //fetch the items and print the results
        itemService.fetchAll().forEach(item -> {
            System.out.printf("Item id: %d Item name: %s Item Price %s %n",item.getItemId(), item.getItemName(), item.getItemPrice());
        });

        ((AbstractApplicationContext)applicationContext).registerShutdownHook();
    }
}